# Pipeline with caching

This project is a part of my work on the [CI Bootcamp](https://gitlab.com/gitlab-com/support/support-training/-/issues/1288). 

## npm ci
> npm ci bypasses a package’s package.json to install modules from a package’s lockfile. This ensures reproducible builds—you are getting exactly what you expect on every install.

[Introducing npm ci for faster, more reliable builds](https://blog.npmjs.org/post/171556855892/introducing-npm-ci-for-faster-more-reliable)